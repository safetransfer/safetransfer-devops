#!/bin/bash

# Function to safely create an overlay network if it doesn't exist
create_overlay_network_if_not_exists() {
  network_name="$1"
  echo "Checking for the existence of the network: ${network_name}"
  if ! docker network inspect "${network_name}" &>/dev/null; then
    echo "Creating overlay network ${network_name}"
    if docker network create --driver overlay "${network_name}"; then
      echo "Successfully created the network: ${network_name}"
    else
      echo "Failed to create the network: ${network_name}"
    fi
  else
    echo "Overlay network ${network_name} already exists"
  fi
}

# Create necessary overlay networks
echo "Creating necessary overlay networks..."
create_overlay_network_if_not_exists myapp-app-db
create_overlay_network_if_not_exists myapp-app-frontend
create_overlay_network_if_not_exists myapp-app-ipfs

echo "Proceeding with deployment..."

# Check if docker-compose.yml.template exists
if [ ! -f docker-compose.yml.template ]; then
    echo "docker-compose.yml.template file not found!"
    exit 1
fi

# Substitute environment variables into the template and deploy
envsubst '$MYSQL_ROOT_PASSWORD' < docker-compose.yml.template > docker-compose.yml

# Manually verify the output
cat docker-compose.yml | grep "mysqladmin"

# Deploy the stack
docker stack deploy -c docker-compose.yml my_stack
