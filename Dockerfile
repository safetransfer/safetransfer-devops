# Use an official Ubuntu as a parent image
FROM ubuntu:20.04

# Install necessary packages
RUN apt-get update && \
    apt-get install -y python3-pip python3-venv openssh-client && \
    pip3 install ansible

# Install Ansible.md collections
RUN ansible-galaxy collection install community.digitalocean

# Set work directory
WORKDIR /ansible

# Copy your Ansible.md playbooks into the Docker image
COPY . .

# Default command: just keep the container running
CMD ["tail", "-f", "/dev/null"]
