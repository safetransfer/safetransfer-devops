## Documentation

- [Getting Started](docs/Getting_Started.md)
- [Project Architecture](docs/Architecture.md)
- [Environment Variables](docs/Envs.md)
- [Deployment Guide](docs/Deployment.md)
- [Ansible Documentation](docs/Ansible.md)
- [CI/CD Documentation](docs/CI_CD_Documentation.md)
- [API Documentation](https://gitlab.com/safetransfer/SafeTransfer-Backend/-/blob/main/docs/docs.md?ref_type=heads)

## Project Screenshots

### Main Interface
![Main Interface](docs/screen1.png)

### File Transfer Process
![File Transfer](docs/screen2.png)
