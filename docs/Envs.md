# Environment Variables Documentation for SafeTransfer

This document outlines all the environment variables used across different environments in the SafeTransfer project. These variables are crucial for the configuration and operation of the application, ensuring secure and efficient deployments.

## Overview

Environment variables are key-value pairs that are used to configure the SafeTransfer application and its deployment processes. They are used to handle sensitive data, configure application settings, and manage access to external services.

## Environment Variables List

Here is a detailed list of all environment variables used in the SafeTransfer project, along with their descriptions.

### Global Variables

These variables are used across all components of the SafeTransfer project.

- **DO_API_TOKEN**
    - **Description**: DigitalOcean API token used for provisioning and managing resources.

- **SSH_KEY**
    - **Description**: SSH key used for secure access to deployment servers and other necessary resources.

- **RUNNER_IP**
    - **Description**: IP address of the GitLab Runner.

- **SSL_MODE**
    - **Description**: Configures SSL mode for PostgreSQL database connections.

### Database Configuration

- **POSTGRES_USER**
    - **Description**: Username for the PostgreSQL database.

- **POSTGRES_PASSWORD**
    - **Description**: Password for the PostgreSQL database.

- **POSTGRES_DB**
    - **Description**: Database name for the PostgreSQL instance.

- **DB_PORT**
    - **Description**: Port number on which the PostgreSQL server is running.

- **DB_NAME**
    - **Description**: Name of the database used by the application.

- **DB_HOST**
    - **Description**: Hostname or IP address of the database server.

### Authentication and Security

- **JWT_SECRET**
    - **Description**: Secret key used to sign JSON Web Tokens for authentication.

- **DROPLET_PRIVATE_KEY**
    - **Description**: Private SSH key used for accessing DigitalOcean droplets.

- **DROPLET_IP**
    - **Description**: IP address of the DigitalOcean droplet used for deployment.

### Docker Registry Credentials

- **DOCKER_USERNAME**
    - **Description**: Username for Docker registry.

- **DOCKER_PASSWORD**
    - **Description**: Password for Docker registry.
