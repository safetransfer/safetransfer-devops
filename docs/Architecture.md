# SafeTransfer Architecture

SafeTransfer follows a microservices architecture, with the backend and frontend developed as separate services. This architecture allows for better scalability, maintainability, and flexibility.

## Backend

The backend is built using Go and integrates with the Ethereum blockchain for user authentication and IPFS for decentralized file storage.

## Frontend

The frontend is developed using React and MetaMask, offering a user-friendly interface for interacting with the SafeTransfer application.

## CI/CD Pipeline

The CI/CD pipeline is powered by GitLab CI/CD, automating the build, test, and deployment processes to ensure consistent and reliable deployments.
