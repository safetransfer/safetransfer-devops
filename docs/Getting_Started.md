# Getting Started with SafeTransfer

Welcome to SafeTransfer! This guide will help you get started with setting up your development environment and deploying the application. SafeTransfer is a comprehensive DevOps project designed to facilitate secure file transfers. It leverages cutting-edge technologies like blockchain for authentication and IPFS for decentralized storage, ensuring robust security and scalability.

## Project Description

SafeTransfer provides a secure platform for transferring files, integrating Ethereum blockchain for user authentication and IPFS for file storage. This setup not only enhances security but also improves the integrity and availability of the data. The project is split into two main components: a backend service built with Go, and a frontend interface developed using React and MetaMask.

## Prerequisites

Ensure you have the following installed:

- Go: version 1.18 or higher
- PostgreSQL: version 14 or higher
- Docker: version 20.10 or higher
- Ethereum wallet: MetaMask or any compatible wallet
- IPFS node: version 0.13 or higher
- Node.js: version 16 or higher
- npm: version 8 or higher
- MetaMask browser extension: latest version

## Setting Up the Development Environment

1. Clone the [SafeTransfer-Backend](https://gitlab.com/safetransfer/SafeTransfer-Backend) and [SafeTransfer-Frontend](https://gitlab.com/safetransfer/SafeTransfer-Frontend) repositories.
2. Set all needed environment variables.
3. Ensure all prerequisites are met.

## Deploying the Application

1. Refer to the [Deployment Guide](Deployment.md) for step-by-step deployment instructions.
2. For detailed CI/CD pipeline information, see the [CI/CD Documentation](CI_CD_Documentation.md).
