# SafeTransfer CI/CD Documentation

SafeTransfer utilizes GitLab CI/CD for automating the deployment process. This document outlines the pipeline configuration and deployment steps.

## Pipeline Overview

The CI/CD pipeline consists of a single stage:

- **Deploy**: Deploys the application to the production environment using Ansible playbooks.

## Configuration

The pipeline is configured using the `.gitlab-ci.yml` file in the SafeTransfer-DevOps repository. The `deploy-job` runs in the `deploy` stage, setting up the GitLab Runner and deploying the application using Ansible playbooks.

## Deployment

The deployment process is triggered when changes are pushed to the `main` branch. It involves setting up the GitLab Runner using the `setup_gitlab-runner.yml` playbook and deploying the application to the production environment using the `deploy.yml` playbook and the `digital_ocean.yml` inventory file.
