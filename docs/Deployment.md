# Deployment Documentation

The deployment of SafeTransfer utilizes Docker Swarm and is automated through Ansible. This streamlined approach ensures a consistent and reliable setup across environments.

## Deployment Overview

1. **SSH Configuration**: Set up SSH access for secure communication with the deployment server.
2. **Environment Setup**: Define necessary environment variables for configuration.
3. **File Distribution**: Distribute essential configuration files and scripts using Ansible.
4. **Deploy Application**: Run the `setup.sh` script to deploy the application using Docker Compose.


## Key Files

- **docker-compose.yml.template**: Template for setting up service containers.
- **setup.sh**: Script for network configuration and service deployment.
- **nginx.conf**: Configuration for the Nginx web server.

## Deployment Steps

1. **Prepare the Server**:

    - Set all required environment variables.

2. **Run Deployment**:
    - Use this command to start the deployment:
      `ansible-playbook -i inventory/digital_ocean.yml playbooks/deploy.yml

3. **Check Services**:
    - Ensure all services are operational by inspecting Docker and accessing the application endpoint.

4. **Debugging**:
    - For any deployment issues, review Docker logs and recheck configurations.