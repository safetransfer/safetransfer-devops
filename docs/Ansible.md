Ansible Documentation

The SafeTransfer project utilizes Ansible for automating deployment and configuration management. This section outlines the structure of the Ansible playbooks and roles, setup instructions, and provides examples of how to run the playbooks.

## Purpose and Structure

### Playbooks

Ansible's playbooks are YAML files that describe the desired state of the system. They are used to manage configurations and deploy applications across various environments. Key playbooks in the SafeTransfer project include:

- **deploy.yml**: Deploys the application to production servers tagged with `prod`.
- **gitlab-runner.yml**: Sets up and configures GitLab Runner on specified hosts.
- **setup_gitlab-runner.yml**: Prepares the local environment for GitLab Runner.

### Roles

Roles are collections of tasks and additional files for configuring host systems. They promote reuse and help organize playbooks. Significant roles in the SafeTransfer project are:

- **deploy**: Manages deployment tasks like copying files, setting up Docker, and executing scripts.
- **docker**: Ensures Docker is installed and configured correctly.
- **gitlab-runner**: Handles the installation, configuration, and registration of GitLab Runner.

## Configuration

### ansible.cfg

This configuration file sets default behaviors for Ansible:
```yml
[defaults]
inventory = ./inventory/hosts.ini
roles_path = ./roles
retry_files_enabled = False
fact_caching = jsonfile
fact_caching_connection = ./cache
collections_paths = ~/.ansible/collections:/usr/share/ansible/collections

```

### Inventory Files

- **hosts.ini**: Defines groups of hosts and their connection details.
- **digital_ocean.yml**: Specifies hosts in the DigitalOcean cloud with dynamic inventory settings.

### Variables

The SafeTransfer project uses variables to store sensitive information and configuration details. The `deploy` role requires the following minimal set of variables:
```yml
droplet_ip: "{{ lookup('env', 'DROPLET_IP') }}"
jwt_secret: "{{ lookup('env', 'JWT_SECRET') }}"
db_host: "{{ lookup('env', 'DB_HOST') }}"
db_password: "{{ lookup('env', 'DB_PASSWORD') }}"
ssl_mode: "{{ lookup('env', 'SSL_MODE') }}"
docker_username: "{{ lookup('env', 'DOCKER_USERNAME') }}"
docker_password: "{{ lookup('env', 'DOCKER_PASSWORD') }}"
postgres_db: "{{ lookup('env', 'POSTGRES_DB') }}"
postgres_user: "{{ lookup('env', 'POSTGRES_USER') }}"
postgres_password: "{{ lookup('env', 'POSTGRES_PASSWORD') }}"
droplet_private_key: "{{ lookup('env', 'DROPLET_PRIVATE_KEY') }}"

```

`gitlab-runner` role:
```yml
gitlab_runner_concurrent: 4  
gitlab_runner_check_interval: 0  
gitlab_runner_session_timeout: 1800  
gitlab_runner_url: "https://gitlab.com/"  
gitlab_runner_token: "{{ gitlab_runner_registration_token }}"
```

These variables are retrieved from environment variables using the `lookup` function. Ensure that the corresponding environment variables are set before running the playbooks.

Note : Don't forget about editing in ansible valut your `gitlab_runner_registration_token`.

## Running Playbooks

To execute the Ansible playbooks, use the `ansible-playbook` command followed by the playbook file and any necessary options. Here are examples of how to run the key playbooks:

```yml
# Deploy the application to production
ansible-playbook -i inventory/digital_ocean.yml playbooks/deploy.yml

# Configure GitLab Runner on the specified hosts
ansible-playbook -i inventory/hosts.ini playbooks/gitlab-runner.yml

```

## Setup and Configuration

To set up and configure Ansible for the SafeTransfer project, follow these steps:

1. **Use mine docker image for Ansible in CI/CD**:  `image: light3739/safe-transfer-deploy:my-ansible-image`
2. **Configure ansible.cfg**: Review and adjust the `ansible.cfg` file according to your environment's requirements. The provided configuration should work for most cases.
3. **Prepare Inventory**: Update the `hosts.ini` and `digital_ocean.yml` files in the `inventory` directory with the appropriate host details and credentials. For DigitalOcean, make sure to set the `DO_API_TOKEN` environment variable with a valid API token. 
4. **Set Environment Variables**: Define the necessary environment variables mentioned in the "Variables" section. These variables should be set in your CI/CD pipeline or in your local environment before running the playbooks.
5. **Run Playbooks**: Execute the desired playbooks using the `ansible-playbook` command, specifying the appropriate inventory file and any required options.

### Note : Don't forget to tag your droplets with tag `PROD`